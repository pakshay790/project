package com.lib;

import java.io.*;



public class FileOperations {

	static String PATH = "E:\\Projects\\LockedMe\\files\\";
	static int i = 1;
	static String[] tempArr;

	public static void AddFile(String filename) {

		try {
			File myObj = new File(PATH + filename);
			if (myObj.createNewFile()) {
				GenLib.displayEmptyline();
				System.out.println(myObj.getName()+" File added Successfully : ");
			} else {
				GenLib.displayEmptyline();
				System.out.println(filename+ " File name already exists");
			}
		} catch (IOException e) {
			System.out.println("An error occurred.");

		}

	}

	public static void DeleteFile(String fileName) {

		try {
			String [] tempArr = allFiles();
			
			int res = GenLib.binarySearch(tempArr, fileName);
			
			if (res == -1)
	            System.out.println(fileName +" File not present in the system");
	        else {
	        	File myObj = new File(PATH + fileName);
				if (myObj.delete()) {
					System.out.println("Deleted the file: " + myObj.getName());
				}
				 else {
						System.out.println(fileName+ " File not found ");
				 }
			}
		} catch (Exception e) {
			System.out.println("An error occurred.");

		}

	}

	public static String[] allFiles() {
 
		 
		File filePath = new File(PATH); //
		//List of all files and directories 
		String[] filesList = filePath.list();
		return filesList; 
		
	}

	public static void sortedFilesASC() {

		File filePath = new File(PATH);
		// List of all files and directories
		tempArr = filePath.list();
		
		if (tempArr.length == 0) {
			System.out.println("No files found in the system");
		} else {
			GenLib.bubbleSortforFile(tempArr);
		}

	}

	public static void searchFile(String fileName) {

		String [] tempArr = allFiles();
		
		int res = GenLib.binarySearch(tempArr, fileName);
		
		if (res == -1)
            System.out.println(fileName +" File not present in the system");
        else
            System.out.println(fileName+" File found");
		
	}

}
