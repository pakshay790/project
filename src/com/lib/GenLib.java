package com.lib;

import java.util.Scanner;

public class GenLib {

	static String strIn;

	public static int returnInputStr() {
		Scanner objIn = new Scanner(System.in);
		strIn = objIn.nextLine();
		return Integer.parseInt(strIn);
	}

	public static int binarySearch(String[] arr, String x) {

		int l = 0, r = arr.length - 1;
		while (l <= r) {
			int m = l + (r - l) / 2;

			int res = x.compareTo(arr[m]);

			// Check if x is present at mid
			if (res == 0)
				return m;

			// If x greater, ignore left half
			if (res > 0)
				l = m + 1;

			// If x is smaller, ignore right half
			else
				r = m - 1;
		}

		return -1;

	}
	
	public static void bubbleSortforFile(String[] tempArr) {
		String temp;
		
		String[] resArr;
		//System.out.println("Strings in sorted order:");
		for (int j = 0; j < tempArr.length; j++) {
	   	   for (int i = j + 1; i < tempArr.length; i++) {
			// comparing adjacent strings
			if (tempArr[i].compareTo(tempArr[j]) < 0) {
				temp = tempArr[j];
				tempArr[j] = tempArr[i];
				tempArr[i] = temp;
			}
		   }
		   System.out.println(j+1 +". "+tempArr[j]);
		}
		
		
	}

	public static void displayDashboard() {

		displayEmptyline();
		System.out.println("Main Menu");
		displayEmptyline();
		System.out.println("1. Add New File");
		System.out.println("2. Delete Exsisting File");
		System.out.println("3. Search Exsisting File");
		System.out.println("4. Close application");
		System.out.println("Enter your Choice : ");
		displayEmptyline();
	}

	public static void displayEnd() {

		displayEmptyline();
		System.out.println("Enter 0 to go back to the Dashboard");
		System.out.println("Enter 9 to close the application");
		System.out.println("Please choose the option from above");
		displayEmptyline();
	}
	
	public static void displayIntro() {
		
		System.out.println("Company Lockers Pvt. Ltd.");
		System.out.println("Developer : Akshay Gajanan Patil");
		System.out.println("Email: pakshay790@gmail.com");
	}
	
	public static void displayEmptyline() {
		System.out.println("");
	}
	
	


}
