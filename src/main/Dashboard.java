package main;
import java.util.Scanner;

import com.lib.FileOperations;
import com.lib.GenLib;

public class Dashboard {
	
	static String tempStr;
	static int tempInt;
	static Scanner objIn = new Scanner(System.in);
	

	public static void main(String[] args) throws InterruptedException {
			
		GenLib.displayIntro();
		
		System.out.println("Files Present in the existing system,");
		GenLib.displayEmptyline();
		
		FileOperations.sortedFilesASC();
		while (true) {
			businessLogic();
		}	
	}
	
	static void businessLogic() {
		
		GenLib.displayDashboard();
		
		tempInt = objIn.nextInt();
		while (true) {
			
			if (tempInt == 1){
				
				System.out.println("Please enter the file name ");
				GenLib.displayEmptyline();
				tempStr = objIn.next();
				FileOperations.AddFile(tempStr);	
				while (true) {
					GenLib.displayEnd();
					tempInt = objIn.nextInt();
					if (tempInt==0) {
						
						businessLogic();
						
					} else if(tempInt==9){
						
						System.out.println("Thanks for Visiting LockedMe.com");
						System.exit(0);
					}
					else {
						System.out.println("Please enter a valid Option!!!!");
					}
				}
				
				
			}else if (tempInt == 2){
				
				System.out.println("Files Present in the existing system,");
				GenLib.displayEmptyline();
				
				FileOperations.sortedFilesASC();
				
				GenLib.displayEmptyline();
				
				
				System.out.println("Please enter the file name to delete");
				GenLib.displayEmptyline();
				tempStr = objIn.next();
				
				FileOperations.DeleteFile(tempStr);
				while (true) {
					GenLib.displayEnd();
					tempInt = objIn.nextInt();
					if (tempInt==0) {
						
						businessLogic();
						
					} else if(tempInt==9){
						System.out.println("Thanks for Visiting LockedMe.com");
						System.exit(0);
					}
					else {
						System.out.println("Please enter a valid Option!!!!");
					}
				}
			}else if (tempInt == 3){
				
				System.out.println("Files Present in the existing system,");
				GenLib.displayEmptyline();
				
				FileOperations.sortedFilesASC();
				
				GenLib.displayEmptyline();
				
				System.out.println("Enter File name to search");
				GenLib.displayEmptyline();
				tempStr = objIn.next();
				
				FileOperations.searchFile(tempStr);
				while (true) {
					GenLib.displayEnd();
					tempInt = objIn.nextInt();
					if (tempInt==0) {
						
						businessLogic();
						
					} else if(tempInt==9){
						System.out.println("Thanks for Visiting LockedMe.com");
						System.exit(0);
					}
					else {
						System.out.println("Please enter a valid Option!!!!");
					}
				}
			}else if (tempInt == 4){
				
				System.out.println("Thanks for Visiting LockedMe.com");
				System.exit(0);
				
			}else {
				
				System.out.println("Please enter a valid option to continue");
				tempInt = objIn.nextInt();
				
			}
		}
	}
	
	
	
	
	
	

}
